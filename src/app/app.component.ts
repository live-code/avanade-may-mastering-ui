import { Component } from '@angular/core';
import { LanguageService } from './core/language.service';

@Component({
  selector: 'ava-root',
  template: `
    <button routerLink="lifecycle">lifecycle</button>
    <button routerLink="uikit1">uikit1</button>
    <button routerLink="uikit2">uikit2</button>
    <button routerLink="viewchildren">viewchildren</button>
    <button routerLink="dynamic">dynamic</button>
    <button routerLink="demo-directives1">directives 1</button>
    <button routerLink="demo-directives2">directives 2</button>
    <button routerLink="demo-pipes">pipes1</button>
    <button routerLink="demo-pipes2">pipes2</button>
    <button routerLink="demo-pipes3">pipes3</button>
    <hr>
    <button (click)="languageService.lang = 'it'">It</button>
    <button (click)="languageService.lang = 'en'">en</button>
    {{languageService.lang}}
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
  constructor(public languageService: LanguageService) {}
}
