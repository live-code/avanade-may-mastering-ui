import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemoViewchildrenComponent } from './demo-viewchildren.component';

const routes: Routes = [{ path: '', component: DemoViewchildrenComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoViewchildrenRoutingModule { }
