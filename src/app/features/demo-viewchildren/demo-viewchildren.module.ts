import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoViewchildrenRoutingModule } from './demo-viewchildren-routing.module';
import { DemoViewchildrenComponent } from './demo-viewchildren.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    DemoViewchildrenComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    DemoViewchildrenRoutingModule,
    FormsModule
  ]
})
export class DemoViewchildrenModule { }
