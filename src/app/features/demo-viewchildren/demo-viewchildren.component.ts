import { Component, ContentChildren, Optional, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { PanelComponent } from '../../shared/components/panel.component';
import { ThemeService } from '../../core/theme.service';

@Component({
  selector: 'ava-demo-viewchildren',
  template: `
    <h2>Grid / row & col</h2>
    <pre>resize screen to see how it works</pre>

    <my-row mq="sm">
      <my-col>ciao ciao</my-col>
      <my-col>lorem ipsum</my-col>
      <my-col>ciao</my-col>
    </my-row>

    <hr>
    <div class="row">
      <div class="col-md">1</div>  
      <div class="col-md">1</div>  
      <div class="col-md">1</div>  
      <div class="col-md">1</div>  
    </div>

      <h2>Accordion</h2>
     <ava-accordion>
        <ava-panel title="one">bla bla1</ava-panel>
        <ava-panel title="two">bla bla2</ava-panel>
        <ava-panel title="three">bla bla3</ava-panel>
      </ava-accordion>
      
      <ava-accordion [openOnce]="false">
        <ava-panel title="1">bla bla1</ava-panel>
        <ava-panel title="2">bla bla2</ava-panel>
        <ava-panel title="3">bla bla3</ava-panel>
      </ava-accordion>
      
  `,
})
export class DemoViewchildrenComponent {
  value: string | undefined;

  constructor(@Optional() private themeService: ThemeService) {
    if(themeService) {
      this.value = this.themeService.value;
    } else {
      this.value = 'themePIppo'
    }
  }
}
