import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoDynamicRoutingModule } from './demo-dynamic-routing.module';
import { DemoDynamicComponent } from './demo-dynamic.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    DemoDynamicComponent
  ],
  imports: [
    CommonModule,
    DemoDynamicRoutingModule,
    SharedModule
  ]
})
export class DemoDynamicModule { }
