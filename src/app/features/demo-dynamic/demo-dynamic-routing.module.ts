import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemoDynamicComponent } from './demo-dynamic.component';

const routes: Routes = [{ path: '', component: DemoDynamicComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoDynamicRoutingModule { }
