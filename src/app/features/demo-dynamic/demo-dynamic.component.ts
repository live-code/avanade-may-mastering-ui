import { Component } from '@angular/core';

@Component({
  selector: 'ava-demo-dynamic',
  template: `
    <h1>Dynamic Layout by JSON</h1>
    
    <h3>With directive</h3>
    <div
      *ngFor="let config of json"
      [avaLoader]="config"
      (action)="doSomething($event)"
    ></div>

    <hr>
    <hr>
    <hr>
    <h3>With ngFor</h3>
    <div *ngFor="let config of json">
      <ava-panel [title]="config.data.title" *ngIf="config.compo === 'panel'"> bla bla {{config.data.city}}</ava-panel>
      <ava-weather [city]="config.data.city" *ngIf="config.compo === 'weather'"></ava-weather>
    </div>
    
  `,
})
export class DemoDynamicComponent  {
  json: any = [
    {
      id: 1,
      compo: 'weather',
      data: { city: 'Milano', unit: 'imperial'},
    },
    {
      id: 2,
      compo: 'panel',
      data: { title: 'profilo' },
      events: ['headerClick']
    },
    {
      id: 1,
      compo: 'weather',
      data: { city: 'Milano' }
    },
  ]

  doSomething(eventInfo: any) {
    console.log('do something in parent', eventInfo)
  }
}
