import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemoDirectives1Component } from './demo-directives1.component';

const routes: Routes = [{ path: '', component: DemoDirectives1Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoDirectives1RoutingModule { }
