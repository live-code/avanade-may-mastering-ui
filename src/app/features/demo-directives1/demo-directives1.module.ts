import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoDirectives1RoutingModule } from './demo-directives1-routing.module';
import { DemoDirectives1Component } from './demo-directives1.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    DemoDirectives1Component
  ],
  imports: [
    CommonModule,
    SharedModule,
    DemoDirectives1RoutingModule,
    FormsModule
  ]
})
export class DemoDirectives1Module { }
