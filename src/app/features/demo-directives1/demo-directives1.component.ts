import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ThemeService } from '../../core/theme.service';
import { UserDirective } from '../../shared/directives/user.directive';

@Component({
  selector: 'ava-demo-directives1',
  template: `
    
    <h2>Basic directives (bg, color, border)</h2>
    <button (click)="themeService.inc()">+</button>
    
    <h1 bg="purple" [color]="value" pad>Hello directives</h1>
    <h3 [color]="value"> h3  </h3>
    <div avaBorder borderType="dashed">ciao</div>
    <div avaBorder borderColor="red">ciao</div>
    
    <hr>
    <h2>Export AS + async directives</h2>
    <div #u="avaUsr" [avaUser]="themeService.padding"></div>
    <div [avaMember]="themeService.padding"></div>
    <div *ngIf="u.text && u.text.length > 5">nome lungo</div>
    
  `,
})
export class DemoDirectives1Component  {
  @ViewChild('u') userRef!: UserDirective;

  city = 'trieste'
  value = 'white'

  constructor(public themeService: ThemeService) {
    setTimeout(() => {
      this.value = 'red';
      console.log(this.userRef.text)
    }, 2000)
  }

}
/*

document.querySelector('.pippo')
  .innerHTML = 'ciao'*/
