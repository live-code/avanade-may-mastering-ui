import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ava-demo-uikit2',
  template: `
    <h1>Chart Examples</h1>
    <ava-temperature-chart [temperatures]="temps"></ava-temperature-chart>
    <ava-chart [config]="cfg"></ava-chart>
  `,
})
export class DemoUikit2Component implements OnInit {
  temps = [10, 20, 30];

  cfg = {
    chart: {
      type: 'bar'
    },
    title: {
      text: 'Fruit Consumption'
    },
    xAxis: {
      categories: ['Apples', 'Bananas', 'Oranges']
    },
    yAxis: {
      title: {
        text: 'Fruit eaten'
      }
    },
    series: [{
      name: 'Jane',
      data: [1, 0, 4]
    }, {
      name: 'John',
      data: [5, 7, 3]
    }]
  };


  constructor() {
    setTimeout(() => {
      this.temps = [30, 10, 40];
      this.cfg = {
        ...this.cfg,
        series: [{
          name: 'Jane',
          data: [21, 0, 44]
        }, {
          name: 'John',
          data: [25, 7, 33]
        }]
      }
    }, 3000)
  }

  ngOnInit(): void {
  }

}
