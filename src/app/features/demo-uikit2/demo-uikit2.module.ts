import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoUikit2RoutingModule } from './demo-uikit2-routing.module';
import { DemoUikit2Component } from './demo-uikit2.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    DemoUikit2Component
  ],
  imports: [
    CommonModule,
    SharedModule,
    DemoUikit2RoutingModule
  ]
})
export class DemoUikit2Module { }
