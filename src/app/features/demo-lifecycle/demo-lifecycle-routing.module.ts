import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemoLifecycleComponent } from './demo-lifecycle.component';

const routes: Routes = [{ path: '', component: DemoLifecycleComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoLifecycleRoutingModule { }
