import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ava-demo-lifecycle',
  template: `
    
    <input type="text" [ngModel]="obj.theme">
    <ava-hello
      [config]="obj"
      [city]="value" [color]="col"></ava-hello>
  
    <ava-weather
      [city]="value"
      [unit]="myUnit"
      [color]="col"
    ></ava-weather>
    
    <button (click)="myUnit = 'metric'">metric</button>
    <button (click)="myUnit = 'imperial'">imperial</button>
    <button (click)="col = 'green'">green</button>
    
  `,
})
export class DemoLifecycleComponent {
  value: string | undefined = 'Trieste'
  myUnit = 'metric'
  col = 'red';
  obj: any = { theme: 'dark', values: [1, 2,3]}

  constructor() {
    setTimeout(() => {
     //this.value = 'Milan';
     this.obj.theme = 'light'
      //this.obj = { ...this.obj, theme: 'light'}
    }, 2000)

  }
}
