export const translation: any = {
  en: {
    'HELLO': 'hi',
    'WELCOME': 'welcome'
  },
  it: {
    'HELLO': 'ciao',
    'WELCOME': 'benvenuto'
  }
}
