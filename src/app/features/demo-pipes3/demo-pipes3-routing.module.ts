import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemoPipes3Component } from './demo-pipes3.component';

const routes: Routes = [{ path: '', component: DemoPipes3Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoPipes3RoutingModule { }
