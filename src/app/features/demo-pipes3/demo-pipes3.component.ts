import { Component, OnInit } from '@angular/core';
import { User } from '../../model/user';

@Component({
  selector: 'ava-demo-pipes3',
  template: `

    {{'HELLO' | translate}}
    {{'WELCOME' | translate}}
    <hr>
    
   <select [(ngModel)]="filters.gender">
     <option value="all">Tutti</option>
     <option value="M">male</option>
     <option value="F">female</option>
   </select>
   <br>
   <input type="text" [(ngModel)]="filters.name" placeholder="search name">
   <input type="number" [(ngModel)]="filters.age" placeholder="search age">
   <br>
   <select [(ngModel)]="filters.sort">
     <option value="ASC">asc</option>
     <option value="DESC">desc</option>
   </select>
   
    <li *ngFor="let u of (users 
                | filterByAge: filters.age 
                | filterByGender: filters.gender
                | filterByText: filters.name
                | sort: filters.sort
    )">
      {{u.name}}
    </li>
    
   <hr>
   <ava-user-list
     [items]="users | filterByAge: filters.age 
                    | filterByGender: filters.gender
                    | filterByText: filters.name
                    | sort: filters.sort"
   ></ava-user-list>
   
   
  `,
})
export class DemoPipes3Component {
  users: User[] = [
    { id: 1, name: 'Michele', age: 30, gender: 'M' },
    { id: 3, name: 'Silvia', age: 20, gender: 'F' },
    { id: 2, name: 'Mich', age: 25, gender: 'M' }
  ];

  filters: Filter = {
    gender: 'all',
    age: 18,
    name: '',
    sort: 'ASC'
  }

}

interface Filter {
  gender: 'M' | 'F' | 'all'
  age: number | undefined,
  name: string;
  sort: 'ASC' | 'DESC'
}

