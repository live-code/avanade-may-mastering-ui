import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoPipes3RoutingModule } from './demo-pipes3-routing.module';
import { DemoPipes3Component } from './demo-pipes3.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { UserListComponent } from './components/user-list.component';


@NgModule({
  declarations: [
    DemoPipes3Component,
    UserListComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    DemoPipes3RoutingModule,
    FormsModule
  ]
})
export class DemoPipes3Module { }
