import { Component, Input, OnInit } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'ava-user-list',
  template: `
    <li *ngFor="let u of items">{{u.name}}</li>
  `,
  styles: [
  ]
})
export class UserListComponent {
  @Input() items: User[] = []

}
