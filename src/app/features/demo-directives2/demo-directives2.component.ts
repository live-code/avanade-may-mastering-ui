import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ava-demo-directives2',
  template: `
    
    <h1>Tooltip</h1>
    <span avaTooltip>Trieste</span>
    <span avaTooltip>Roma</span>
    
    <hr>
    <h1>Url</h1>
    visit  <span avaUrl="http://www.google.com">Google</span>!!

    <hr>
    <h1>Stop Propagation</h1>
    <div pad bg="red" (click)="parent()">
      <div pad bg="cyan" stopPropagation (click)="child()">1</div>
    </div>
  `,
})
export class DemoDirectives2Component  {

  parent() {
    console.log('parent')
  }

  child() {
    console.log('child')
  }
}
