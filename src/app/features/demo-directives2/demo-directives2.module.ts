import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoDirectives2RoutingModule } from './demo-directives2-routing.module';
import { DemoDirectives2Component } from './demo-directives2.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    DemoDirectives2Component
  ],
  imports: [
    CommonModule,
    SharedModule,
    DemoDirectives2RoutingModule
  ]
})
export class DemoDirectives2Module { }
