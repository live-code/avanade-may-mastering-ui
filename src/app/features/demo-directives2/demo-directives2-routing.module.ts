import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemoDirectives2Component } from './demo-directives2.component';

const routes: Routes = [{ path: '', component: DemoDirectives2Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoDirectives2RoutingModule { }
