import { Component } from '@angular/core';

@Component({
  selector: 'ava-demo-uikit1',
  template: `
    <ava-leaflet
      [coords]="values" [zoom]="zoom"
      (markerClick)="doSomething($event)"
    ></ava-leaflet>
    <ava-leaflet *ngIf="visible" [coords]="[43, 13]"></ava-leaflet>
    
    <button (click)="zoom = zoom + 1">+</button>
    <button (click)="zoom = zoom - 1">-</button>
  `,

})
export class DemoUikit1Component  {
  values = [51.5, -0.09]
  zoom = 16;
  visible = false;

  constructor() {
    setTimeout(() => {
      this.values = [43, 13]
    }, 2000)
  }

  doSomething(data: any) {
    console.log(data)
    this.visible = true;
  }
}
