import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoUikit1RoutingModule } from './demo-uikit1-routing.module';
import { DemoUikit1Component } from './demo-uikit1.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    DemoUikit1Component
  ],
  imports: [
    CommonModule,
    SharedModule,
    DemoUikit1RoutingModule
  ]
})
export class DemoUikit1Module { }
