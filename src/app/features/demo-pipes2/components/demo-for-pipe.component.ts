import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ava-rate',
  template: `
    <i class="fa fa-star"
       *ngFor="let item of values"></i>
  `,
})
export class DemoForPipeComponent  {
  @Input() values: any[] = []
}
