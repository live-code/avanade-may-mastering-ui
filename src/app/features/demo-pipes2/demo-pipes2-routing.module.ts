import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemoPipes2Component } from './demo-pipes2.component';

const routes: Routes = [{ path: '', component: DemoPipes2Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoPipes2RoutingModule { }
