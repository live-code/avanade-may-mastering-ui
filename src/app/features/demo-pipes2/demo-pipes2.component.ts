import { Component, OnInit } from '@angular/core';
import { format, formatDistance, formatRelative, subDays } from 'date-fns'
import { HttpClient } from '@angular/common/http';
import { filter, map } from 'rxjs';
import { MapquestPipe } from '../../shared/pipes/mapquest.pipe';
import { FilterByTextPipe } from '../../shared/pipes/filter-by-text.pipe';

@Component({
  selector: 'ava-demo-pipes2',
  template: `

    <ava-rate [values]="rate | intToArray"></ava-rate>
    
    <h2>Date FNS</h2>
    {{today | date: 'dd MMM'}}
    {{today | dayname}}
    {{today | timesago}}

    <h2>Mapquest pipe</h2>
    <img [src]="city | mapquest" width="100%">
    <img [src]="'rome' | mapquest" width="100%">
    ...
    {{(user$ | async)?.name}}
    
    <hr>
    {{city | temperature | async}}
    {{2 | userById | async}}
    
  `,
})
export class DemoPipes2Component{
  city = 'Trieste'
  today = 1613551279000
  rate = 4
  user$ = this.http.get<any>('https://jsonplaceholder.typicode.com/users/1')
    .pipe(
      map(user => ({ name: user.name.toUpperCase()}))
    )

  constructor(private http: HttpClient) {}

}
