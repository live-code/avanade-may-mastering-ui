import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoPipes2RoutingModule } from './demo-pipes2-routing.module';
import { DemoPipes2Component } from './demo-pipes2.component';
import { SharedModule } from '../../shared/shared.module';
import { DemoForPipeComponent } from './components/demo-for-pipe.component';


@NgModule({
  declarations: [
    DemoPipes2Component,
    DemoForPipeComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    DemoPipes2RoutingModule
  ]
})
export class DemoPipes2Module { }
