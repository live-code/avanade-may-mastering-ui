import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ava-demo-pipes',
  template: `
    <h1>Memory Example Pipe</h1>
    <form #f="ngForm" (submit)="update(f.value)" >
      <input type="number" ngModel name="value">
      <select ngModel name="unit">
        <option value="">Select unit</option>
        <option value="mb">Megabytes</option>
        <option value="bytes">bytes</option>
      </select>
      <button type="submit">CHANGE</button>
    </form>
    
    <hr>
    MEMORY: {{ value | formatTo:  unit  }} <br>
    
  `,
})
export class DemoPipesComponent {
  value: number | undefined
  unit: 'mb' | 'bytes' = 'mb'

  update(formData: any) {
    this.value = formData.value;
    this.unit = formData.unit;
  }
}
