import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoPipesRoutingModule } from './demo-pipes-routing.module';
import { DemoPipesComponent } from './demo-pipes.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    DemoPipesComponent
  ],
  imports: [
    CommonModule,
    DemoPipesRoutingModule,
    FormsModule,
    SharedModule
  ]
})
export class DemoPipesModule { }
