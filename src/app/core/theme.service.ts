import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ThemeService {
  value = 'dark'
  padding = 1;

  inc() {
    this.padding +=1;
  }
}
