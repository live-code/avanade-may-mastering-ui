import { Directive, ElementRef, HostBinding, Renderer2, ViewContainerRef } from '@angular/core';
import { HelloComponent } from '../components/hello.component';

@Directive({
  selector: 'div____' // remove ____ to work
})
export class DivDirective {
  @HostBinding('style.margin') margin = '10px'

  constructor(
    private el: ElementRef,
    private renderer: Renderer2,
  ) {
    this.renderer.setStyle(
      this.el.nativeElement,
      'border',
      '2px solid blue'
    )

  }

}
