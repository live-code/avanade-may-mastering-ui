import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[avaBorder]'
})
export class BorderDirective {
  @Input() borderType = 'solid';
  @Input() borderColor = 'black';
  @HostBinding('style.margin') margin = '10px'

  @HostBinding('style.border') get border() {
    return `3px ${this.borderType} ${this.borderColor}`;
  }

}
