import { Directive, ElementRef, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Directive({
  selector: '[avaUser]',
  exportAs: 'avaUsr'
})
export class UserDirective {
  text: string | undefined;

  @Input() set avaUser(id: number) {
    this.http.get<any>('https://jsonplaceholder.typicode.com/users/' + id)
      .subscribe(res => {
        this.el.nativeElement.innerText = res.name
        this.text = res.name;
      })
  }

  constructor(
    private http: HttpClient,
    private el: ElementRef<HTMLElement>
  ) {
  }


}
