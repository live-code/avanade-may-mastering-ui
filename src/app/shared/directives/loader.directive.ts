import { ComponentRef, Directive, EventEmitter, Input, Output, ViewContainerRef } from '@angular/core';
import { PanelComponent } from '../components/panel.component';
import { WeatherComponent } from '../components/weather.component';

@Directive({
  selector: '[avaLoader]'
})
export class LoaderDirective {
  @Input('avaLoader') config: any;
  @Output() action = new EventEmitter<any>()

  constructor(private view: ViewContainerRef) {}

  ngOnInit() {
    const compo: ComponentRef<any> = this.view.createComponent(DICT[this.config.compo])
    for(let key in this.config.data) {
      compo.instance[key] = this.config.data[key]
    }
    if (this.config.events) {
      for(let key of this.config.events) {
        compo.instance[key].subscribe((res: any) => {
          this.action.emit({ event: key, data: res, compo: this.config.compo })
        })
      }
    }

  }

}

const DICT: any = {
  'panel': PanelComponent,
  'weather': WeatherComponent
}
