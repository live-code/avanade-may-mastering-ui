import { Directive, Host, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[bg]',
})
export class BgDirective {
  @Input() bg = 'red'
  // @HostBinding() class = 'alert alert-danger'
  // @HostBinding() innerHTML = '<em>ciao</em>'
  @HostBinding('style.fontSize') fontSize = '20px'

  @HostBinding('style.background') get background() {
    return this.bg;
  }
}
