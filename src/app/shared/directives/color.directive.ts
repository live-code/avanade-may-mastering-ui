import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[color]'
})
export class ColorDirective {
  @Input() set color(val: string) {
    this.el.nativeElement.style.color = val;
  }

  constructor(private el: ElementRef<HTMLElement> ) {
 /*  if (this.el.nativeElement.tagName === 'H2') {
     throw new Error('non puoi applicarlo su H2')
   }*/
  }

}
