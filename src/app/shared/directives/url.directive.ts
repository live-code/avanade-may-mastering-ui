import { Directive, ElementRef, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[avaUrl]'
})
export class UrlDirective {
  @HostBinding('style.cursor') cursor = 'pointer';
  @HostBinding('style.color') color = 'orange';
  @Input() avaUrl: string | undefined;

  @HostListener('click')
  clickMe() {
    window.open(this.avaUrl)
  }

/*

  constructor(private el: ElementRef<HTMLElement>) {
    this.el.nativeElement
      .addEventListener('click', () => {
        window.open(this.avaUrl)
      })
  }
*/

}
