import { Directive, HostBinding } from '@angular/core';
import { ThemeService } from '../../core/theme.service';

@Directive({
  selector: '[pad]'
})
export class PadDirective {
  @HostBinding('style.padding') get pad() {
    return this.themeService.padding + 'px';
  }

  @HostBinding('style.margin') margin = '40px'

  constructor(private themeService: ThemeService) {

  }
}
