import { Directive, ElementRef, Input, Renderer2, Sanitizer } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';

@Directive({
  selector: '[avaMember]'
})
export class MemberDirective {
  @Input() avaMember: number | undefined;

  constructor(
    private http: HttpClient,
    private el: ElementRef<HTMLElement>,
    private renderer: Renderer2,
  ) {}

  ngOnChanges() {
    this.http.get<any>('https://jsonplaceholder.typicode.com/users/' + this.avaMember)
      .subscribe(res => {
        const name = this.renderer.createText(res.name)
        this.renderer.appendChild(this.el.nativeElement, name)
        this.renderer.setStyle(this.el.nativeElement, 'fontSize', '20px')
      })
  }

}
