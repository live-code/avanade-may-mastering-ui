import { Directive, ElementRef, HostListener, ViewContainerRef } from '@angular/core';
import { HelloComponent } from '../components/hello.component';
import { LeafletComponent } from '../components/leaflet.component';
import { WeatherComponent } from '../components/weather.component';

@Directive({
  selector: '[avaTooltip]'
})
export class TooltipDirective {

  constructor(
    private el: ElementRef<HTMLElement>,
    private view: ViewContainerRef
  ) {
  }

  @HostListener('mouseover')
  mouseOver() {
    // this.view.createComponent(HelloComponent)
    const weatherRef = this.view.createComponent(WeatherComponent)
    weatherRef.instance.city = this.el.nativeElement.innerText
  }

  @HostListener('mouseout')
  mouseOut() {
    this.view.clear()
  }


}
