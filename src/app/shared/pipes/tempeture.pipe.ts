import { Pipe, PipeTransform } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Meteo } from '../model/meteo';
import { map, Observable } from 'rxjs';

const BASEPATH = 'https://api.openweathermap.org/data/2.5/weather'
@Pipe({
  name: 'temperature'
})
export class TempeturePipe implements PipeTransform {

  constructor(private http: HttpClient) {}

  transform(city: string): Observable<string> {
     return this.http.get<Meteo>(`${BASEPATH}?q=${city}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
      .pipe(
        map(meteo => `${meteo.main.temp}°`)
      )
  }

}
