import { Pipe, PipeTransform } from '@angular/core';
import { format } from 'date-fns';

@Pipe({
  name: 'dayname'
})
export class DaynamePipe implements PipeTransform {

  transform(value: number | Date): string {
    return format(value, "eeee");
  }

}
