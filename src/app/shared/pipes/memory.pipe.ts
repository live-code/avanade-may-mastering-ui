import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatTo'
})
export class MemoryPipe implements PipeTransform {

  transform(
    val: number | undefined,
    type: 'mb' | 'bytes'
  ): string {
    if (!val) return '0'

    switch (type) {
      case 'mb':
        return `${val * 1000} mb`
      case 'bytes':
        return `${val * 1000000} mb`
    }
  }

}
