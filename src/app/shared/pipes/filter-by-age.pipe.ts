import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../../model/user';

@Pipe({
  name: 'filterByAge'
})
export class FilterByAgePipe implements PipeTransform {

  transform(users: User[], minAge?: number ): any {
    if (!minAge) return users;
    return users.filter(u => u.age >= minAge)
  }

}
