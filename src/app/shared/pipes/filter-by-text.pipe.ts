import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../../model/user';

@Pipe({
  name: 'filterByText'
})
export class FilterByTextPipe implements PipeTransform {

  transform(users: User[], textToSearch: string): User[] {
    return users.filter(u => {
      return u.name.toLowerCase().includes(textToSearch.toLowerCase())
    })
  }

}
