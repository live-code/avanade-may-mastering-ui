import { Pipe, PipeTransform } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs';

@Pipe({
  name: 'userById'
})
export class UserByIdPipe implements PipeTransform {
  constructor(private http: HttpClient) {}

  transform(id: number) {
    return this.http.get<any>(`https://jsonplaceholder.typicode.com/users/${id}`)
      .pipe(
        map(u => u.name)
      )
  }

}
