import { Pipe, PipeTransform } from '@angular/core';
import { translation } from '../../features/demo-pipes3/utils/translations';
import { LanguageService } from '../../core/language.service';

@Pipe({
  name: 'translate',
  pure: false
})
export class TranslatePipe implements PipeTransform {
  constructor(private languageService: LanguageService) {}

  transform(key: string): unknown {
    return translation[this.languageService.lang][key];
  }

}


