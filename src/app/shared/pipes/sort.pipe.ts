import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../../model/user';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(users: User[], order: 'ASC' | 'DESC' = 'ASC'): User[] {
    const result = users.sort((a: User, b: User) => a.name.localeCompare(b.name));
    return order === 'ASC' ? result : result.reverse();
  }

}
