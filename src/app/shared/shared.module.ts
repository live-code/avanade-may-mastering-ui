import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HelloComponent } from './components/hello.component';
import { WeatherComponent } from './components/weather.component';
import { LeafletComponent } from './components/leaflet.component';
import { ChartComponent } from './components/chart.component';
import { TemperatureChartComponent } from './components/temperature-chart.component';
import { PanelComponent } from './components/panel.component';
import { AccordionComponent } from './components/accordion.component';
import { RowComponent } from './components/row.component';
import { ColComponent } from './components/col.component';
import { BgDirective } from './directives/bg.directive';
import { PadDirective } from './directives/pad.directive';
import { BorderDirective } from './directives/border.directive';
import { HighlightDirective } from './directives/highlight.directive';
import { ColorDirective } from './directives/color.directive';
import { UserDirective } from './directives/user.directive';
import { MemberDirective } from './directives/member.directive';
import { DivDirective } from './directives/div.directive';
import { UrlDirective } from './directives/url.directive';
import { StopPropagationDirective } from './directives/stop-propagation.directive';
import { TooltipDirective } from './directives/tooltip.directive';
import { LoaderDirective } from './directives/loader.directive';
import { MemoryPipe } from './pipes/memory.pipe';
import { MapquestPipe } from './pipes/mapquest.pipe';
import { DaynamePipe } from './pipes/dayname.pipe';
import { TimesagoPipe } from './pipes/timesago.pipe';
import { FilterByGenderPipe } from './pipes/filter-by-gender.pipe';
import { FilterByAgePipe } from './pipes/filter-by-age.pipe';
import { FilterByTextPipe } from './pipes/filter-by-text.pipe';
import { SortPipe } from './pipes/sort.pipe';
import { IntToArrayPipe } from './pipes/int-to-array.pipe';
import { TranslatePipe } from './pipes/translate.pipe';
import { TempeturePipe } from './pipes/tempeture.pipe';
import { UserByIdPipe } from './pipes/user-by-id.pipe';



@NgModule({
  declarations: [
    // components
    HelloComponent,
    WeatherComponent,
    LeafletComponent,
    ChartComponent,
    TemperatureChartComponent,
    PanelComponent,
    AccordionComponent,
    RowComponent,
    ColComponent,
    // directives
    BgDirective,
    PadDirective,
    BorderDirective,
    HighlightDirective,
    ColorDirective,
    UserDirective,
    MemberDirective,
    DivDirective,
    UrlDirective,
    StopPropagationDirective,
    TooltipDirective,
    LoaderDirective,
    MemoryPipe,
    MapquestPipe,
    DaynamePipe,
    TimesagoPipe,
    FilterByGenderPipe,
    FilterByAgePipe,
    FilterByTextPipe,
    SortPipe,
    IntToArrayPipe,
    TranslatePipe,
    TempeturePipe,
    UserByIdPipe,

  ],
  exports: [
    // components
    HelloComponent,
    LeafletComponent,
    ChartComponent,
    WeatherComponent,
    TemperatureChartComponent,
    PanelComponent,
    AccordionComponent,
    RowComponent,
    ColComponent,
    // directives
    BgDirective,
    PadDirective,
    BorderDirective,
    HighlightDirective,
    ColorDirective,
    UserDirective,
    MemberDirective,
    DivDirective,
    UrlDirective,
    StopPropagationDirective,
    TooltipDirective,
    LoaderDirective,
    MemoryPipe,
    MapquestPipe,
    DaynamePipe,
    TimesagoPipe,
    FilterByGenderPipe,
    FilterByAgePipe,
    FilterByTextPipe,
    SortPipe,
    IntToArrayPipe,
    TranslatePipe,
    TempeturePipe,
    UserByIdPipe,
  ],
  imports: [
    CommonModule,
  ]
})
export class SharedModule { }
