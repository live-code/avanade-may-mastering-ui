import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Meteo } from '../model/meteo';

@Component({
  selector: 'ava-weather',
  template: `
    <p>weather: {{city}}</p>
    
    <div *ngIf="meteo" [style.color]="color">
      {{meteo.main.temp}} ° 
      <br>
      <img [src]="'http://openweathermap.org/img/w/' + meteo.weather[0].icon +'.png'" alt="">
    </div>
  `,
  styles:[`
  
  `]
})
export class WeatherComponent implements OnChanges {
  @Input() city: string | undefined;
  @Input() unit: string = 'metric';
  @Input() color = 'blue';

  meteo: Meteo | undefined;

  constructor(private http: HttpClient) {}


  ngOnChanges(changes: SimpleChanges): void {
    const { city, unit, color } = changes;

    if ((city || unit) && (this.city)) {
      this.getMeteo()
    }
  }

  ngOnInit() {
    if (!this.meteo) {
      this.getMeteo()
    }
  }

  getMeteo() {
    this.http.get<Meteo>(`https://api.openweathermap.org/data/2.5/weather?q=${this.city}&units=${this.unit}&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
      .subscribe(res => {
        this.meteo = res;
      })
  }

}
