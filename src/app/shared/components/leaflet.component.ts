import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import * as L from 'leaflet';

var myMarker = L.icon({
  iconUrl: 'assets/map-marker-icon.png',

  iconSize:     [50, 50], // size of the icon
  shadowSize:   [50, 64], // size of the shadow
  iconAnchor:   [25, 50], // point of the icon which will correspond to marker's location
  shadowAnchor: [4, 62],  // the same for the shadow
  popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});
@Component({
  selector: 'ava-leaflet',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div #host class="map"></div>
  `,
  styles: [`
    .map { width: 100%; height: 180px; background-color: purple }
  `]
})
export class LeafletComponent {
  @ViewChild('host', { static: true }) myHost!: ElementRef<HTMLElement>;
  @Input() coords: any;
  @Input() zoom: number = 5;
  @Input() showMarkers = true;
  @Output() markerClick = new EventEmitter<any>();
  map!: L.Map;
  marker!: L.Marker

  ngOnInit() {
    if (!this.map) {
      this.initMap()
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    const { coords, zoom } = changes;
    if (coords && coords.firstChange) {
        this.initMap()
    }

    if(coords) {
      this.map.flyTo(this.coords)
      this.marker.setLatLng(this.coords)
    }
    if(zoom) {
      this.map.setZoom(this.zoom)
    }
  }

  initMap(): void {
    this.map = L.map(this.myHost.nativeElement)
      .setView(this.coords, 5);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);

    this.marker = L.marker(this.coords, {icon: myMarker}).addTo(this.map)
      .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
      .openPopup()
      .on('click', () => {
        this.markerClick.emit(this.marker.getLatLng())
      });
  }


}
