import { ChangeDetectionStrategy, Component, HostBinding, Optional } from '@angular/core';
import { RowComponent } from './row.component';

@Component({
  selector: 'my-col',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <ng-content></ng-content>
  `,
})
export class ColComponent {
  @HostBinding() get class() {
    return 'col-' + (this.parent?.mq || 'sm')
    //return this.parent ? 'col-' + this.parent.mq : ''
    // return 'col-' + this.parent?.mq
  }

  constructor(@Optional() private parent: RowComponent) {
  }

}
