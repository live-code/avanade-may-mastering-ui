import {
  AfterContentInit,
  AfterViewInit,
  Component,
  ContentChildren, Input,
  OnInit,
  QueryList,
  ViewChildren
} from '@angular/core';
import { PanelComponent } from './panel.component';

@Component({
  selector: 'ava-accordion',
  template: `
    <div style="border: 1px solid black; padding: 20px">
      <ng-content></ng-content>
    </div>
  `,
})
export class AccordionComponent implements AfterContentInit {
  @Input() openOnce = true;
  @ContentChildren(PanelComponent) panels!: QueryList<PanelComponent> ;

  ngAfterContentInit(): void {
    this.panels.toArray()[0].isOpen = true;
    this.panels.toArray().forEach(p => {
      p.headerClick
        .subscribe(() => {
          if (this.openOnce) {
            this.closeAll()
            p.isOpen = true;
          } else {
            p.isOpen = !p.isOpen
          }
        })
    })
  }

  closeAll() {
    this.panels.toArray().forEach(p => {
      p.isOpen = false;
    })
  }

}
