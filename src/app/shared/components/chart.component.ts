import { ChangeDetectionStrategy, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import * as Highcharts from 'highcharts';
import { config } from 'rxjs';
@Component({
  selector: 'ava-chart',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div #container style="width:100%; height:400px;"></div>
  `,
})
export class ChartComponent  {
  @ViewChild('container') host!: ElementRef<HTMLElement>;
  @Input() config!: any;
  chart!: Highcharts.Chart;

  ngOnChanges() {
    if (this.chart) {
      this.chart.update(this.config)
    }
  }

  ngAfterViewInit(): void {
    this.chart = (Highcharts as any)
      .chart(this.host.nativeElement, this.config);
  }


}
