import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'ava-hello',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <p [style.color]="color" >
      hello {{city}}! {{config?.theme}} {{opts}}
    </p>
  `,
})
export class HelloComponent implements OnChanges {
  @Input() city: string | undefined;
  @Input() color = 'black';
  @Input() config: any;
  @Input() set options(val: any) {
    // ...
    this.opts = val;
  }
  opts: any;

  ngOnChanges(changes: SimpleChanges) {
    // console.log('HTTP request', changes)
  }

}
