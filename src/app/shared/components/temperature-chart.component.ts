import { Component, Input, OnChanges, OnInit } from '@angular/core';

@Component({
  selector: 'ava-temperature-chart',
  template: `
    <ava-chart [config]="cfg"></ava-chart>
  `,
})
export class TemperatureChartComponent implements OnChanges {
  cfg: any = {
    chart: {
      type: 'spline'
    },
    title: {
      text: 'Temperature'
    },
    xAxis: {
      categories: ['LUN', 'MAR', 'MER']
    },
    yAxis: {
      title: {
        text: 'Temperatures'
      }
    },
    series: [{
      name: 'Days',
      data: []
    }]
  };

  @Input() temperatures: number[] = [];

  ngOnChanges(): void {
    this.cfg = {
      ...this.cfg,
      series: [{
        name: 'Days',
        data: this.temperatures
      }]

    }
  }

}
