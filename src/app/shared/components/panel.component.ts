import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ava-panel',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div>
      <div class="title" (click)="headerClick.emit()">{{title}}</div>
      <div class="body" *ngIf="isOpen">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: [`
    .title { background-color: black; color: white; padding: 10px}
    .body { border: 1px solid black; padding: 10px}
  `]
})
export class PanelComponent  {
  @Input() title: string | undefined
  @Input() isOpen = false;
  @Output() headerClick = new EventEmitter()
}
