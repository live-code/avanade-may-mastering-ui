import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'lifecycle', loadChildren: () => import('./features/demo-lifecycle/demo-lifecycle.module').then(m => m.DemoLifecycleModule) },
  { path: 'uikit1', loadChildren: () => import('./features/demo-uikit1/demo-uikit1.module').then(m => m.DemoUikit1Module) },
  { path: 'uikit2', loadChildren: () => import('./features/demo-uikit2/demo-uikit2.module').then(m => m.DemoUikit2Module) },
  { path: 'viewchildren', loadChildren: () => import('./features/demo-viewchildren/demo-viewchildren.module').then(m => m.DemoViewchildrenModule) },
  { path: '', redirectTo: 'lifecycle', pathMatch: 'full'},
  { path: 'dynamic', loadChildren: () => import('./features/demo-dynamic/demo-dynamic.module').then(m => m.DemoDynamicModule) },
  { path: 'demo-directives1', loadChildren: () => import('./features/demo-directives1/demo-directives1.module').then(m => m.DemoDirectives1Module) },
  { path: 'demo-directives2', loadChildren: () => import('./features/demo-directives2/demo-directives2.module').then(m => m.DemoDirectives2Module) },
  { path: 'demo-pipes', loadChildren: () => import('./features/demo-pipes/demo-pipes.module').then(m => m.DemoPipesModule) },
  { path: 'demo-pipes2', loadChildren: () => import('./features/demo-pipes2/demo-pipes2.module').then(m => m.DemoPipes2Module) },
  { path: 'demo-pipes3', loadChildren: () => import('./features/demo-pipes3/demo-pipes3.module').then(m => m.DemoPipes3Module) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
